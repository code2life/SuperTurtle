/**
 *  a simple di, configure your dependencies here and use DI.generateScope()
 * */

var dependency = {
    common :{
        cheerio : require('cheerio'),
        _ : require('underscore')
    },
    crawler : {
        auth : require('./mapping').Map.crawlerAuth
    },
    analyzer : {
        mailer : require('nodemailer')
    },
    cron : {
        mailer : require('nodemailer')
    }
};

dependency.generateScope = function(obj,module){
    for (var d in dependency.common) {
        obj[d] = dependency.common[d];
    }
    for(var d in dependency[module]) {
        obj[d] = dependency[module][d];
    }
    return obj;
};

module.exports = dependency;