/**
 * Functions save in db
 * */
var EasyMongo = require('../../common').EasyMongo;

var requestName = "iview";
initRequest(requestName);

/**
 *  @param request chain series name
 * */
function initRequest(name) {
    EasyMongo.remove('request', {series: name}, function () {
        EasyMongo.add('request', {
            series: name,
            url: "http://hln2058p:1234/iview/logout.jsp?logonas=1",
            option: {},
            method: 'GET',
            sequence: 1,
            filter: function () {
                console.log('IView REQ Filter : step 1 complete...');
            }
        }, function (err, id) {
            console.log(id);
        });

        EasyMongo.add('request', {
            series: name,
            url: "http://hln2058p:1234/iview/login.jsp?logonas=1",
            option: {},
            method: 'GET',
            sequence: 2,
            filter: function () {
                console.log('IView REQ Filter : step 2 complete...');
            }
        }, function (err, id) {
            console.log(id);
        })

        EasyMongo.add('request', {
            series: name,
            url: "https://ssoidp.oocl.com/idp/2w9UL/resumeSAML11/idp/startSSO.ping",
            method: 'GET',
            option: {},
            sequence: 3,
            filter: function (err, res, body, next) {
                next.option.headers.Authorization = this.auth.iview;
                console.log('IView REQ Filter : step 3 complete...');
            }
        }, function (err, id) {
            console.log(id);
        })

        EasyMongo.add('request', {
            series: name,
            url: "http://ssoidp.oocl.com/idp/startSSO.ping?PartnerSpId=OOCL:SP:SAML1&IdpAdapterId=" +
            "MSADAdapter&TargetResource=http%3A%2F%2Fhklxdv60%3A1234%2Fiview%2Flogin.jsp%3Flogonas%3D1",
            method: 'POST',
            sequence: 4,
            option: {
                headers: {
                    "Authorization": ""
                }
            },
            filter: function (err, res, body, next) {
                console.log('IView REQ Filter : step 4 complete...');
                next.option.headers.Authorization = this.auth.iview;
                next.url = res.headers.location;
            }
        }, function (err, id) {
            console.log(id);
        })

        EasyMongo.add('request', {
            series: name,
            url: null,
            method: 'GET',
            sequence: 5,
            option: {
                headers: {
                    "Authorization": ""
                }
            },
            filter: function (err, res, body, next) {
                var $ = this.cheerio.load(res.body);
                var token = $("input").val();
                var target = $("input[name='TARGET']").val();
                var url = $("form").attr("action");
                next.url = url;
                next.option.form = {TARGET: target, SAMLResponse: token};
                next.option.headers.Authorization = this.auth.iview;
                console.log('IView REQ Filter : step 5 complete...');
            }
        }, function (err, id) {
            console.log(id);
        });

        EasyMongo.add('request', {
            series: name,
            url: null,
            method: 'POST',
            sequence: 6,
            option: {
                headers: {
                    "Authorization": ""
                }
            },
            filter: function (err, res, body, next) {
                var $ = this.cheerio.load(body);
                next.option.form = {opentoken: $("input").val()}
                this.addCookie(next, "USER_ID=" + this.auth.iviewId, "http://hln2058p:1234/iview");
                this.addCookie(next, "USER_NAME=" + $("input").val(), "http://hln2058p:1234/iview");
                console.log('IView REQ Filter : step 6 complete...');
            }
        }, function (err, id) {
            console.log(id);
        })

        EasyMongo.add('request', {
            series: name,
            url: "http://hln2058p:1234/iview/login.jsp?logonas=1",
            method: "POST",
            option: {},
            sequence: 7,
            filter: function () {
                console.log('IView REQ Filter : step 7 complete...');
            }
        }, function (err, id) {
            console.log(id);
        })

        EasyMongo.add('request', {
            series : name,
            url: "http://hln2058p:1234/iview/servlet/ViewDir?apps=wms&dirname=/home/wmsprd/wmsapp/logs/exception&sort=t",
            method: 'GET',
            option: {},
            next: null,
            sequence : 8,
            filter: function () {
                console.log('IView REQ Filter : step 8 complete...Redirect to analyzer data builder');
            }
        }, function (err, id) {
            console.log(id);
        });
    });
}