/**
 * Functions save in db
 * */
var EasyMongo = require('../../common').EasyMongo;

var collectionName = "iview-data";
initMeta(collectionName);

function initMeta(name) {
    EasyMongo.remove("meta", {collectionName: name}, function () {
        EasyMongo.add('meta', {
            name: "WMS IView Exception",
            collectionName: name,
            columns: [
                {
                    columnName: "exceptionKey",
                    displayName: "ExceptionKey",
                    editable: false,
                    dataType: "array",
                    defaultValue: null,
                    isDisplay: false,
                    filter: function (data) {
                        return data;
                    },
                    searchable: false
                },
                {
                    columnName: "isRaw",
                    displayName: "isRaw",
                    editable: false,
                    dataType: "string",
                    defaultValue: "0",
                    isDisplay: false,
                    filter: null,
                    searchable: false
                },
                {
                    columnName: "release",
                    displayName: "出现版本",
                    editable: true,
                    dataType: "string",
                    defaultValue: null,
                    isDisplay: false,
                    filter: null,
                    width: 100,
                    searchable: false
                },
                {
                    columnName: "times",
                    displayName: "本周/全部",
                    editable: false,
                    dataType: "string",
                    defaultValue: "1/1",
                    isDisplay: true,
                    filter: null,
                    width: 90,
                    searchable: false
                },
                {
                    columnName: "owner",
                    displayName: "负责人",
                    editable: true,
                    width: 120,
                    dataType: {
                        setting: {multi: false},
                        options: {
                            "": "All Members",
                            "Alex-Sun": "Alex Sun", "Frank-Yuan": "Frank Yuan", "Lun-Liu": "Lun Liu",
                            "Mark-Shi": "Mark Shi", "Mars-Huang": "Mars Huang", "Martin-Mao": "Martin Mao",
                            "Minya-Sun": "Minya Sun", "Rain-Li": "Rain Li", "Riley-Lu": "Riley Lu"
                        }
                    },
                    defaultValue: null,
                    isDisplay: true,
                    filter: null,
                    searchable: true
                },
                {
                    columnName: "status",
                    displayName: "状态",
                    width: 110,
                    editable: true,
                    dataType: {
                        setting: {multi: false, defaultOption: "define"},
                        options: {
                            "": "全部",
                            define: "已发现",
                            progress: "处理中",
                            finish: "已解决",
                            again: "再次出现",
                            ignore: "不再关注"
                        }
                    },
                    defaultValue: "define",
                    isDisplay: true,
                    filter: null,
                    searchable: true
                },
                {
                    columnName: "createdAt",
                    displayName: "发现时间",
                    editable: false,
                    dataType: "date",
                    defaultValue: Date.now,
                    isDisplay: true,
                    width: 150,
                    filter: "yyyy/MM/dd hh:mm",
                    deFilter: function (data) {
                        return new Date(data).valueOf();
                    },
                    searchable: true
                },
                {
                    columnName: "lastAppear",
                    displayName: "最近出现",
                    editable: false,
                    dataType: "date",
                    width: 150,
                    defaultValue: Date.now,
                    isDisplay: true,
                    filter: "yyyy/MM/dd hh:mm",
                    deFilter: function (data) {
                        return new Date(data).valueOf();
                    },
                    searchable: true
                },
                {
                    columnName: "targetTime",
                    displayName: "目标时间",
                    editable: true,
                    width: 150,
                    dataType: "date",
                    defaultValue: null,
                    isDisplay: true,
                    filter: "yyyy/MM/dd hh:mm",
                    deFilter: function (data) {
                        return new Date(data).valueOf();
                    },
                    searchable: true
                },
                {
                    columnName: "exceptionType",
                    displayName: "异常类型",
                    editable: false,
                    dataType: "string",
                    width: 220,
                    defaultValue: null,
                    isDisplay: true,
                    filter: null,
                    searchable: false
                },
                {
                    columnName: "trace",
                    displayName: "栈轨迹",
                    width: 185,
                    editable: false,
                    dataType: "string",
                    extra: 15,
                    defaultValue: null,
                    isDisplay: true,
                    filter: null,
                    searchable: false
                }
            ],
            pageSize: 20,
            crud: [0, 1, 1, 0],  //[create, read, update, delete]
            sortKey: [{"lastAppear": "desc"}, {"targetTime": "desc"}],
            cronFunction: {
                freq: "0 1 * * * *",
                //freq: "*/10 * * * * *",
                monitor: "*/3 * * * * *",
                worker: function () {
                    var ALL_MAILER = "WPSUPP";
                    var self = this;
                    var current = new Date().valueOf();
                    self.EasyMongo.find('iview-data', {}, function (err, docs) {
                        if (docs && docs.length != 0) {
                            docs.forEach(function (obj) {
                                if (obj.targetTime && (current > new Date(obj.targetTime).valueOf()) &&
                                    !(obj.status == "finish" || obj.status == "ignore")) {
                                    var tempHtml = "<h2>目标时间内未解决异常</h2><h3>异常类型 : " + obj.exceptionType + "</h3><h4>已出现 : " + obj.times +
                                        "次(本周/全部)&nbsp;&nbsp;&nbsp;&nbsp;最近发生时间:" + obj.lastAppear + "</h4>" +
                                        "<p>异常信息 : " + "<span>" + obj.trace + "</span></p>";
                                    sendMail(obj.owner || ALL_MAILER, tempHtml);
                                }
                            });
                        }
                    });
                    function sendMail(sendTo, html) {
                        sendTo = sendTo.replace("-", ".");
                        var monitorMail = {
                            from: "Exception_Eye@oocl.com",
                            to: sendTo + "@oocl.com",
                            subject: "IView Exception NOTICE",
                            generateTextFromHTML: true,
                            html: html
                        };

                        var transport = self.mailer.createTransport("SMTP", {
                            host: "smtpapp1",
                            port: 25
                        });
                        console.log("Mail Content : " + monitorMail);
                        transport.sendMail(monitorMail, function (error, response) {
                            if (error) {
                                console.log("Monitor Mailer : ERROR " + error);
                            } else {
                                console.log("Monitor Mailer : Message sent: " + response.message + "\n");
                            }
                            transport.close();
                        });
                    }
                }
            }
        }, function () {
        });
    });
}