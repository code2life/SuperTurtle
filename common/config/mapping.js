/**
 *  message mapping, configure message here
 * */
exports.Map = {
    errorCode: {
        // start with 00 means wrong usage
        paramError: {code: "0001", description: "调用参数有误"},
        privilegeError: {code: "0002", description: "操作权限不足"},
        dirtyDataError: {code: "0003", description: "数据库存在脏数据"},

        // start with 05 means server wrong
        basicServerError: {code: "0500", description: "因服务器程序存在缺陷出错"}
    },

    constString: {
        startTurtleSuccess: "启动成功"
    },

    crawlerAuth: {
        iview: "Basic eWFuZ2pvOll3Ynp5bW0xOTk0fg==",   //id:pwd -> base64
        iviewId: "yangjo"
    }
};
