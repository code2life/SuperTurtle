var mongoApi = require('../lib/mongo-rest');
var collectionRoute = require('./collectionRoute');
var turtleRoute = require('./turtleRoute');

module.exports = function (app) {

    app.get('/', function (req, res) {
        res.send("set up success!");
    });

    /**
     *  all test success, open one comment to test
     * */
    app.get('/test', function (req, res) {
        // EasyMongo.add("user", [{userName: "test"},{userName1:'test1'}], function (err, _id) {
        //    if (err) {
        //        console.log(err);
        //        res.send(err);
        //    }
        //    else {
        //        res.send(_id);
        //    }
        //});

        // EasyMongo.update("user",{userName:"test1"}, {userName:'test2'} , function (err, count) {
        //    if (err) {
        //        console.log(err);
        //        res.send(err);
        //    }
        //    else {
        //        res.send({updated:count});
        //    }
        //});

        // EasyMongo.remove("user",{userName:"test2"}, function (err, count) {
        //    if (err) {
        //        console.log(err);
        //        res.send(err);
        //    }
        //    else {
        //        res.send({deleted:count});
        //    }
        //});

        //EasyMongo.find("user",{userName1:"test1"}, function (err, docs) {
        //    if (err) {
        //        console.log(err);
        //        res.send(err);
        //    }
        //    else {
        //        res.send(docs);
        //    }
        //});

        //EasyMongo.listCollections(function(err,names){
        //    if(err) res.send(err);
        //    else res.send(names);
        //});
        //EasyMongo.removeCollection('user',function(err,result){
        //    if(err) res.send(err);
        //    else res.send(result);
        //});

    });

    app.get('/displayData/:name',collectionRoute.displayDataPage);
    app.post('/displayData/:id',collectionRoute.displayConfigData);

    app.get('/collection/:id',collectionRoute.findCollectionData);
    app.post('/collection/:id',collectionRoute.addCollectionData);
    app.put('/collection/:id',collectionRoute.updateCollectionData);
    app.delete('/collection/:id',collectionRoute.removeCollectionData);
    app.get('/download/:id',collectionRoute.exportCollectionData);

    app.get("/turtle/start", turtleRoute.autoStartTurtle);
    //mongoApi.registerMongoRestApi(app);
};
