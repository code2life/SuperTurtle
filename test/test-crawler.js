var crawler = require("../lib/crawler").Crawler;
var tool = require("../common").Toolkit;

var analyzer = function(data){
    tool.listAttributs(data[0].header);
};

var test = new crawler({
    name : "test",
    requestChain : {url : 'http://www.baidu.com',method : 'Get',option:{},next:null,filter:function(err,res){}},
    start : new Date(),
    end : new Date().valueOf() + 5000,
    freq : "*/1 * * * * *",
    monitor : "* * * * * *",
    synGroup : 1
});

test.setAnalyzer(analyzer);
