
/**
 *  Enhance Date Format
 * */
Date.prototype.Format = function (fmt) {
    var o = {
        'M+': this.getMonth() + 1,
        'd+': this.getDate(),
        'h+': this.getHours(),
        'm+': this.getMinutes(),
        's+': this.getSeconds(),
        'q+': Math.floor((this.getMonth() + 3) / 3),
        'S': this.getMilliseconds()
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp('(' + k + ')').test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)));
    return fmt;
};

Number.prototype.Format = function (decimals){
    if (isNaN(this.valueOf())) { return 0};
    if (this.valueOf()=='') { return 0};

    var snum = new String(this.valueOf());
    var sec = snum.split('.');
    var whole = parseFloat(sec[0]);
    var result = '';

    if(sec.length > 1){
      var dec = new String(sec[1]);
      dec = String(parseFloat(sec[1])/Math.pow(10,(dec.length - decimals)));
      dec = String(whole + Math.round(parseFloat(dec))/Math.pow(10,decimals));
      var dot = dec.indexOf('.');
      if(dot == -1){
          dec += '.';
          dot = dec.indexOf('.');
      }
      while(dec.length <= dot + decimals) { dec += '0'; }
      result = dec;
    } else{
      var dot;
      var dec = new String(whole);
      dec += '.';
      dot = dec.indexOf('.');
      while(dec.length <= dot + decimals) { dec += '0'; }
      result = dec;
    }
    return result;
}


/**
 *  List attributes for object, parse to json format
 * */
var listAttributes = function(obj,recursive) {
    if(!recursive) {
        console.log("{");
        for(var i in obj){
            if( typeof obj[i] == 'function' || typeof obj[i] == 'object') {
                console.log("    " + i + " : \"" + typeof obj[i] + "\"");
            } else {
                console.log("    " + i + " : \"" + obj[i] + "\"");
            }
        }
        console.log("}");
    } else {
        listRecursive(obj,0);
    }
};


function listRecursive(obj,count) {
    var str = "";
    for(var i = 0;i<count*4;i++) {
        str += " ";
    }
    console.log(str+"{");
    for(var i in obj){
        if(typeof obj[i] == 'object') {
            console.log(str+ "    " + i+ " : ");
            listRecursive(obj[i], ++count);
        } else if( typeof obj[i] == 'function' ){
            console.log(str + "    " + i + " : " + obj[i].toString().replace("\"","'"));
        } else if( typeof obj[i] == 'string'){
            console.log(str + "    " + i + " : \"" + obj[i] + "\"");
        } else {
            console.log(str + "    " + i + " : " + obj[i]);
        }
    }
    console.log(str+"}");
}

exports.listAttributs = listAttributes;