/**
 * Functions save in db
 * */
var EasyMongo = require('../../common').EasyMongo;

var crawlerName = "iview-crawler";
initCrawler(crawlerName,"iview");

/**
 *  @param crawler name,request chain series name
 * */
function initCrawler(name,reqChain) {
    EasyMongo.remove("crawler", {name:name }, function () {
        EasyMongo.add('crawler', {
            name: name,
            requestChain: reqChain,
            start: new Date(),
            freq: "0 59 23 * * *",
            monitor: "0 * * * * *",
            synGroup: 1,
            timeZone: "Asia/Chongqing",
            dataBuilder: function (cachedData, cookieArray, callback) {
                var crawlerResult = [];
                var urls = [];
                var _self = this;
                cachedData.forEach(function (segment) {
                    var $ = _self.cheerio.load(segment.body);
                    $("table td a[href^='DownloadFile']").each(function () {
                        var dateInfo = "";
                        ($(this).attr("href").match(/=(\d+)\.log/)) && (dateInfo = RegExp.$1);
                        var info = new Date(dateInfo.substring(0,4) +"-"+ dateInfo.substring(4,6) +"-" + dateInfo.substring(6,8)).valueOf();
                        if(new Date().valueOf() - info <= 86400000) {
                            console.log($(this).attr("href"));
                            urls.push("http://hln2058p:1234/iview/servlet/" + $(this).attr("href"));
                        }
                    });
                });
                var j = _self.request.jar();
                this._.each(cookieArray, function (obj) {
                    j.setCookie(_self.request.cookie(obj.key + "=" + obj.value), obj.uri);
                });
                var cursor = 0;
                //todo first time,all data, else, get yesterday data
                (function seriesDownload() {
                    if (cursor < urls.length) {
                        _self.request(urls[cursor], {jar: j}, function (err, res) {
                            if (!err) {
                                /* ensure it is a log file and not empty or meaningless*/
                                if(res.body.length> 10) {
                                    var tempData = res.body.split(/\d\d\d\d\/\d\d\/\d\d\s\d\d:\d\d:\d\d\s-\s/);
                                    if(tempData.length > 1) {
                                        var tempTime = res.body.match(/\d\d\d\d\/\d\d\/\d\d\s\d\d:\d\d:\d\d\s-\s/g);
                                        tempTime.forEach(function(e,i){
                                            Array.prototype.push.call(crawlerResult,e + tempData[i+1]);
                                        });
                                    } else {
                                        crawlerResult.push(res.body);
                                    }
                                    console.log("IView Download Process : downloading log..." + cursor);
                                }
                                cursor++;
                            } else {
                                console.log("IView Download Process : ERROR " + err);
                            }
                            seriesDownload();
                        });
                    } else {
                        callback(crawlerResult);
                    }
                })();
            }
        }, function () {
        });
    });
}