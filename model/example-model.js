/**
 * Functions save in db
 * */
var EasyMongo = require('../common').EasyMongo;

// 1. build request
function r() {
    EasyMongo.remove('request', {series: "test"}, function () {
        EasyMongo.add('request', {
            series: "test",
            url: "http://www.baidu.com",
            option: {},
            method: 'GET',
            sequence: 1,
            filter: function () {
                console.log('TEST REQ Filter : step 1 complete...');
            }
        }, function (err, id) {
            console.log(id);
        });

    });
}


// 2.build crawler
function c() {
    EasyMongo.remove("crawler", {name: "test-c"}, function () {
        EasyMongo.add('crawler', {
            name: "test-c",
            requestChain: "test",
            start: new Date(),
            freq: "*/30 * * * * *",
            monitor: "* * * * * *",
            synGroup: 1,
            timeZone: "Asia/Chongqing"
        }, function () {
        });
    });
}

function m() {
    EasyMongo.remove("meta", {collectionName: "test-data"}, function () {
        EasyMongo.add('meta', {
            name: "Test DATA",
            collectionName: "test-data",
            columns: [
                {
                    columnName: "origin",
                    displayName: "OriginData",
                    editable: false,
                    dataType: "string",
                    defaultValue: null,
                    isDisplay: true,
                    searchable: false
                },
                {
                    columnName: "originData",
                    displayName: "DATA",
                    editable: true,
                    dataType: "date",
                    defaultValue: Date.now,
                    isDisplay: true,
                    filter: "yyyy/MM/dd hh:mm",
                    searchable: true
                }
            ],
            pageSize: 20,
            crud: [1, 1, 1, 1],
            cronFunction: null,
            sortKey: [["lastAppear", "decs"],["targetTime","asc"]]
        }, function () {
        });
    });
}

function e() {
    EasyMongo.remove('executor', {name: 'test'}, function () {
        EasyMongo.add('executor', [{
            name: "test",
            col: "origin",
            method: function (data) {
                console.log("TEST Executor : Save Original Data.");
                return data;
            }
        }
        ], function (err, ids) {
        });
    });
}

function a() {
    EasyMongo.remove('analyzer', {name: "test-a"}, function () {
        EasyMongo.add('analyzer', {
            name: "test-a",   // id
            meta: "test-data",  //save data collection
            executorSeries: 'test', //executor collection
            method: function () {
                console.log("Target Working...");
            },
            cron: {
                start: new Date(),
                freq: "0 * * * * *",
                monitor: "* * * * * *"
            }
        }, function (err, id) {
            console.log(id);
        });
    });
}

m();  //meta
e();  //executor
r();  //requests
c();  //crawler
a();  //analyzer
//Start -> GET : http://localhost:3001/turtle/start?crawler=test-c&analyzer=test-a

db.meta.insert({
    name: "Meta-Data",
    collectionName: "meta.column",
    columns: [
        {
            columnName: "columnName",
            displayName: "列名",
            editable: true,
            dataType: "string",
            defaultValue: null,
            isDisplay: true,
            filter: null,
            searchable: true
        },
        {
            columnName: "displayName",
            displayName: "显示名",
            editable: true,
            dataType: "string",
            defaultValue: null,
            isDisplay: true,
            filter: null,
            searchable: true
        },
        {
            columnName: "editable",
            displayName: "可编辑",
            editable: true,
            dataType: "bool",
            defaultValue: true,
            isDisplay: true,
            filter: null,
            searchable: false
        },
        {
            columnName: "dataType",
            displayName: "数据类型",
            editable: true,
            dataType: {
                setting: {multi: false},
                options: {string: "字符串", date: "日期", func: "函数", array: "数组", enums: "枚举"}
            },
            defaultValue: "string",
            isDisplay: true,
            filter: null,
            searchable: false
        },
        {
            columnName: "defaultValue",
            displayName: "默认值",
            editable: true,
            dataType: "string",
            defaultValue: null,
            isDisplay: true,
            filter: null,
            searchable: false
        },
        {
            columnName: "filter",
            displayName: "过滤器",
            editable: true,
            dataType: 'function',
            defaultValue: null,
            isDisplay: true,
            filter: null,
            searchable: false
        },
        {
            columnName: "searchable",
            displayName: "可搜索",
            editable: true,
            dataType: "bool",
            defaultValue: false,
            isDisplay: true,
            filter: null,
            searchable: false
        },
    ],
    pageSize: 20,
    crud: [1, 1, 1, 1],
    cronFunction: null,
    sortKey: "columnName"
});  //just a example. recursive now not available
