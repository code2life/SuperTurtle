var Mongo = require('./Connection.js');
var ObjectID = require('mongodb').ObjectID;

var query = function () {
};

query.prototype.add = function (collectionName, object, callback) {
    Mongo(function (err, db) {
        if (err) {
            callback(err, null);
            return;
        }
        if (object instanceof Array) {
            db.collection(collectionName).insertMany(object, {serializeFunctions: true}, function (err, r) {
                if (!!r) {
                    console.log('EasyMongo : query end. [inserted into '+ collectionName +':  ' + r.insertedIds + "]");
                    callback(err, r.insertedIds);
                }
                else {
                    console.log('EasyMongo : query end. Inserted ERROR.');
                    callback(err, null);
                }
                //db.close();
            });
        } else {
            db.collection(collectionName).insertOne(object, {serializeFunctions: true}, function (err, r) {
                if (!!r) {
                    console.log('EasyMongo : query end. [inserted into ' + collectionName + ':  ' + r.insertedId + "]");
                    callback(err, r.insertedId);
                }
                else {
                    console.log('EasyMongo : query end. Inserted ERROR.');
                    callback(err, null);
                }
                //db.close();
            });
        }
    });
};

query.prototype.find = function (collectionName, condition, option, callback) {
    if(condition._id) condition._id = ObjectID(condition._id);
    Mongo(function (err, db) {
        if (err) {
            callback(err, null);
            return;
        }
        db.collection(collectionName).find(condition, option).toArray(function (err, docs) {
            if (!!docs) {
                console.log('EasyMongo : query end. [found in ' + collectionName +':  ' + docs.length + " items]");
                callback(err, docs);
            }
            else {
                console.log('EasyMongo : query end. Read ERROR.');
                callback(err, null);
            }
            //db.close();
        });
    });
}

query.prototype.update = function (collectionName, condition, object, callback) {
    if(condition._id) condition._id = ObjectID(condition._id);
    Mongo(function (err, db) {
        if (err) {
            callback(err, null);
            return;
        }
        db.collection(collectionName).updateMany(condition, {$set: object}, function (err, r) {
            if (!!r) {
                console.log('EasyMongo : query end. [updated ' + collectionName +':  ' + r.matchedCount + " items]");
                callback(err, r.matchedCount);
            }
            else {
                console.log('EasyMongo : query end. Updated NONE.');
                callback(err, null);
            }
            //db.close();
        });
    });
};

query.prototype.delete = function (collectionName, condition, callback) {
    if(condition._id) condition._id = ObjectID(condition._id);
    Mongo(function (err, db) {
        if (err) {
            callback(err, null);
            return;
        }
        db.collection(collectionName).deleteMany(condition, function (err, r) {
            if (!!r) {
                console.log('EasyMongo : query end. [deleted ' + collectionName +':  ' + r.deletedCount + " items]");
                callback(err, r.deletedCount);
            }
            else {
                console.log('EasyMongo : query end. Delete NONE.');
                callback(err, null);
            }
            //db.close();
        });
    });
};
exports.Query = query;