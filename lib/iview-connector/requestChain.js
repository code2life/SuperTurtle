var request = require("./lib/request")
var _ = require("underscore");

/* for store cookies */
var j = request.jar();
var jarArray = [];

var startChain = function (current) {
    if (current.method == 'GET') {
        request.get(current.url, current.option, function (err, res, body) {
            current.filter(err, res, body, current.next);
            setCookie(res.headers, current.next);
            if (!!current.next) {
                startChain(current.next);
            }
        });
    } else if (current.method == 'POST') {
        request.post(current.url, current.option, function (err, res, body) {
            current.filter(err, res, body, current.next);
            setCookie(res.headers, current.next);
            if (!!current.next) {
                startChain(current.next);
            }
        });
    }
};

var setCookie = function (header, next) {
    if (!!next) {
        _.each(header['set-cookie'], function (cookies) {
            _.each(cookies.split(";"), function (obj) {
                if ((obj.split("=")[0] == " Path") || (obj.split("=")[0] == "Path")) {
                    next.url.match(/(.+:\/\/[\d|\w|.|-]*:?\d*)/);
                    var host = RegExp.$1;

                    var uri = host + obj.split("=")[1].replace(";", "");

                    var use = false;
                    _.each(jarArray, function (obj) {
                        if (obj.key == cookies.split(";")[0].split("=")[0]) {
                            use = true;
                            obj.value = cookies.split(";")[0].split("=")[1];
                        }
                    })
                    if (!use) {
                        jarArray.push({
                            key: cookies.split(";")[0].split("=")[0],
                            value: cookies.split(";")[0].split("=")[1],
                            uri: uri
                        });
                    }
                }
            });
        });
        j = request.jar();
        _.each(jarArray, function (obj) {
            j.setCookie(request.cookie(obj.key + "=" + obj.value), obj.uri);
        })
        next.option.jar = j;
    }
};

/**
 *  cookie array
 * */
exports.cookies = jarArray;

/**
 *  start request chain from current option
 *  @param: request option
 * */
exports.startChain = startChain;


/**
 *  setCookie from response header
 *  @param: response headers, next option variable
 * */
exports.setCookie = setCookie;

/**
 * add certain customized cookie
 * @param : next request option, cookie like (a=b), scope uri
 * */
exports.addCookie = function addCookie(next, cookie, uri) {

    var use = false;
    _.each(jarArray, function (obj) {
        if (obj.key == cookie.split("=")[0]) {
            use = true;
            obj.value = cookie.split("=")[1];
        }
    })
    if (!use) {
        jarArray.push({
            key: cookie.split("=")[0],
            value: cookie.split("=")[1],
            uri: uri
        });
    }

    j = request.jar();
    _.each(jarArray, function (obj) {
        j.setCookie(request.cookie(obj.key + "=" + obj.value), obj.uri);
    })
    if (!!next) {
        next.option.jar = j;
    }
}