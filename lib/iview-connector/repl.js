var readline = require('readline');
var connector = require('./index');

var account = {};

var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

rl.question("Input Username:",function(ans){
    account.username  = ans;
    rl.question("Input Password:",function(ans){
        account.password  = ans;
        rl.close();
    });
});

rl.on('close', function() {
    connector.setAccount(account.username,account.password);
    connector.requestData();
});