var env = process.env.NODE_ENV || 'development';
var cfg = require('./configuration.json')[env];
module.exports = cfg;
