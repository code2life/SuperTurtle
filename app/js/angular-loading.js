(function(){
    angular.module('ngLoadingSpinner', ['angularSpinner'])
        .directive('usSpinner',   ['$http', '$rootScope' ,function ($http, $rootScope){
            return {
                link: function (scope, elm, attrs)
                {
                    //scope.isLoading = function () {
                    //    var postReqs = _.filter($http.pendingRequests,function(req){
                    //        return  req.method == 'POST';
                    //    })
                    //    return postReqs.length > 0;
                    //};

                    scope.$watch('isLoading', function (loading)
                    {
                        if(loading){
                            elm.removeClass('ng-hide');
                        }else{
                            elm.addClass('ng-hide');
                        }
                    });
                }
            };

        }]);
}).call(this);