var di = require("../common").DI;
var EasyMongo = require("../common").EasyMongo;
var cron = require("../common/cron").cronProcess;

exports.startBasicCron = function () {
    EasyMongo.find("meta", {}, function (err, docs) {
        if (docs && docs.length != 0) {
            console.log("Model Cron : Set up model Cron Job.");
            docs.forEach(function (obj) {
                var e = obj.cronFunction;
                if (e && e instanceof Object && e.worker && e.worker.code) {
                    var scope = {
                        EasyMongo: EasyMongo
                    };
                    di.generateScope(scope, "cron");

                    var code = e.worker.code;
                    code = code.substring(code.indexOf("{")+1, code.lastIndexOf("}"));

                    e.worker = new Function(code);
                    e.scope = scope;
                    new cron(e);
                }
            });
        } else {
            console.log("Model Cron : ERROR " + err);
        }
    })
};