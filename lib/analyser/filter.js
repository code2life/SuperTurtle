var cheerio = require('cheerio');

var filter = function(data,filterArray){
    if(!!data.body) {
        data = data.body;
    }
    try{
        if(filterArray instanceof Array) {
            filterArray.forEach(function(e){
                data = doingFilter(e,data);
            });
        } else {
            data = doingFilter(filterArray,data);
        }
        console.log("Analyzer Filter : Done...Data after filtering: " + data.substring(0,200));
        return data;
    }catch(error) {
        console.log("Analyzer Filter : Filter ERROR. check your config.");
        console.error(error);
        return data;
    }
};


function doingFilter(e,data){
    switch(e.charAt(0)) {
    case "R" :
        var temp = new RegExp(e.substring(e.indexOf('/')+1,e.lastIndexOf('/')),
        e.substring(e.lastIndexOf('/')+1,e.indexOf('$')));
        data.match(temp);data = RegExp["$"+e.charAt(e.length - 1)];
        break; // regex. eg : R/^\\s+(\\w)/gi$1

    case "S" :
        if(e.charAt(1) == '(') {
            var start = e.substring(2,e.length-1).split(",")[0];
            var end = e.substring(2,e.length-1).split(",")[1];
            if(isNaN(parseInt(end))) {
                data = data.substring(parseInt(start));
            } else {
                data = data.substring(parseInt(start),parseInt(end));
            }
        } else if(e.charAt(1) == '['){
            var symbol = e.substring(3,e.lastIndexOf(",")-1);
            var cursor = e.substring(e.lastIndexOf(",")+1,e.length -1 );
            data = data.split(symbol)[parseInt(cursor)];
        } else {
            console.log("Analyzer Filter : Filter Illegal. check your config.");
        }
        break; //substring,split S(0,10) | S[-,2]
    case "$" :
        var $ = cheerio.load(data);
        var method = e.substring(e.lastIndexOf('.')+1,e.lastIndexOf("("));
        var param = e.substring(e.lastIndexOf('(')+2,e.lastIndexOf(")")-1);
        if(param.length == 0) {
            data = $(e.substring(3,e.lastIndexOf('.')-2))[method]();
        } else {
            data = $(e.substring(3,e.lastIndexOf('.')-2))[method](param);
        }
        break; //jquery  $("#x[a!='c']").attr('xx')
    case "F" :
        if(e.indexOf("'") != -1 || e.indexOf('"') != -1) {
            data = new Date(data).Format(e.substring(3,e.length-2));
        } else {
            new Number(data).toFixed(parseInt(e.substring(2,e.length-1)));
        }
        break; //format  date and number F('yyyy-mm-dd') F(5)
    case "+" :
        if(e.match(/\|\d$/)) {
            var pos = parseInt(e.substring(e.lastIndexOf("|")+1,e.length));
            data = data.substring(0,pos) + e.substring(1,e.lastIndexOf("|")) + data.substring(pos,data.length);
        } else {
            data +=  e.substring(1);
        }
        break; //concat certain string +xxx|0-length
    default : console.log("Analyzer Filter : Filter Illegal. check your config.");break;
    }
    return data;
}


exports.filter = filter;