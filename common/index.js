/**
 * common utils and modules for develop
 * */
var config = require('./config');
var Mapping = require('./config/mapping').Map;
var DI = require('./config/di');
var EasyMongo = require('../lib/mongo-rest').EasyMongo;
var Tools = require('./tool');


/**
 * common DB API. usage: EasyMongo.add/remove/fin...(parameters,callback);
 * */
var mongoAPI = new EasyMongo({
    db: config.database.db,
    poolSize: config.database.poolSize,
    host: config.database.host,
    port: config.database.port
});
exports.EasyMongo = mongoAPI;

/**
 *  common Const Data Mapping
 * */
exports.Mapping = Mapping;

/**
 *  common tools
 * */
exports.Toolkit = Tools;

/**
 *  Dependency Injection , config dependencies in mapping.js
 * */
exports.DI = DI;

process.on('SIGINT', function () {
    mongoAPI.exit();
    process.exit(0);
});

process.on('uncaughtException', function (err) {
    console.log('Watchman : UncaughtException. ' + err);
    console.error(err.stack);
});