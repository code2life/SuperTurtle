var easyREPL = require("../lib/easy-repl/repl").REPL_Mode;

var test = new easyREPL();
test.command({
    test:[
        {'-h --help':function(a){
            console.log(a);
        }},
        {'-s --start':function(b){
            console.log(b);
        }},
        {'none': function(){
            console.log('exe');
        }}
    ],
    'node':[
        {'-h --help':function(){
            console.log("sDD");
        }},
        {'-s --start':function(){
            console.log("sCS");
        }}
    ],
    'exit':[
        {'none' : function(){
            process.exit(0);
        }}
    ]
});

/* comment following codes to test command mode */
test.question("q1:" , function(data) {
    console.log(data);
}).question("q2:",function(data){
    console.log(data);
    test.question("q3:" , function(data) {
        console.log(data);
    }).question("q4:",function(data){
        console.log(data);
    });
});