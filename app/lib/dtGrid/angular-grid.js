/**
 *  @description dtGrid constants and factory, for create instance and register APIs
 * */
(function () {
    "use strict";
    /* default configs of dt grid */
    angular.module('dt.grid', [])
        .constant("gridConfig", {
            id: null,
            lang: 'en',
            ajaxLoad: true,
            loadAll: false,
            loadURL: '',
            datas: [],
            check: false,
            exportFileName: 'datas',
            tableStyle: '',
            tableClass: 'table table-bordered table-hover table-responsive',
            showHeader: true,
            gridContainer: 'dtGridContainer',
            toolbarContainer: 'dtGridToolBarContainer',
            tools: 'refresh|faseQuery|advanceQuery|export[excel,csv,pdf,txt]|print',
            exportURL: '/collection/export',
            pageSize: 20,
            pageSizeLimit: [20, 50, 100]
        })

        .constant("columnConfig", {
            id: 'field',
            title: 'field',
            type: 'string',
            format: null,
            otype: null,
            oformat: null,
            columnStyle: '',
            columnClass: '',
            headerStyle: '',
            headerClass: '',
            hide: false,
            hideType: '',
            extra: true,
            codeTable: null,
            fastQuery: false,
            fastQueryType: '',
            advanceQuery: true,
            'export': true,
            print: true,
            resolution: null
        })
        /* construct grid and return dt.grid instance */
        .factory("dtGrid", grid);

    grid.$inject = ['$document', 'gridConfig', 'columnConfig', 'gridService'];

    function grid($document, gridConfig, columnConfig, gridService) {
        var gridAPI = {
            init: function (grid, column) {
                return new gridInstance(grid, column);
            }
        };
        var gridInstance = function (grid, column) {
            //initialized options and origin data
            var self = this;
            this.columnOption = [];
            for (var i = 0; i < columnConfig.length; i++) {
                this.columnOption.push($.extend({}, columnConfig, column))
            }
            this.gridOption = $.extend({}, gridConfig, grid);
            //if no id set by option, get one unique id
            this.gridOption.id || (this.gridOption.id = gridService.uuid());
            //define grid attributes
            this.gridObj = {
                init: {
                    //工具条是否初始化加载
                    toolsIsInit: false,
                    //打印窗体是否初始化
                    printWindowIsInit: false,
                    //导出窗体是否初始化
                    exportWindowIsInit: {},
                    //快速查询窗体是否初始化
                    fastQueryWindowIsInit: false,
                    //高级查询窗体是否初始化
                    advanceQueryWindowIsInit: false
                },
                //页面参数对象
                pager: {
                    //每页显示条数
                    pageSize: 0,
                    //开始记录数
                    startRecord: 0,
                    //当前页数
                    nowPage: 0,
                    //总记录数
                    recordCount: 0,
                    //总页数
                    pageCount: 0
                },
                //表格参数对象
                option: self.gridOption,
                //原始数据集
                originalDatas: self.gridOption.datas,
                //基础数据集
                baseDatas: null,
                //展现数据集
                exhibitDatas: null,
                //排序参数
                sortParameter: {
                    //排序列编号
                    columnId: '',
                    //排序类型：0-不排序；1-正序；2-倒序
                    sortType: 0
                },
                //排序缓存的原生数据
                sortOriginalDatas: null,
                //参数列表
                parameters: null,
                //快速查询的参数列表
                fastQueryParameters: null,
                //快速查询缓存的原生数据
                fastQueryOriginalDatas: null,
                //高级查询的参数列表
                advanceQueryParameter: {
                    //高级查询条件信息
                    advanceQueryConditions: null,
                    //高级查询排序信息
                    advanceQuerySorts: null
                },
                //打印列
                printColumns: null,
                //导出列
                exportColumns: null,
                //导出数据
                exportDatas: null,
            }
        };

        gridInstance.prototype.load = function () {
        }

        return gridAPI;
    }
})();

/**
 *  @description common services of dtGrid
 * */
(function () {
    "use strict";
    /* common service of grid */
    angular.module('dt.grid')
        .service("gridService", ['$rootScope', '$document', function ($rootScope, $document) {
            this.uuid = function () {
                return 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                    return v.toString(16);
                });
            };
            console.log("grid service init..");
        }]);
})();

/**
 *  @description dtGrid controller, handle events, display control
 * */
(function () {
    "use strict";
    /* grid controller */
    angular.module('dt.grid')
        .controller("dtGridCtrl", ['$scope', 'dtGrid', function ($scope,gridAPI) {
            console.log(gridAPI);
            console.log("grid ctrl init..");
        }]);
})();

/**
 *  @description dtGrid compiler, compile directive, create html instance of grid
 * */
(function () {
    "use strict";
    /* compile dt-grid directive */
    angular.module('dt.grid').directive("dtGrid", gridDirective);

    gridDirective.$inject = ['$rootScope', '$document', 'dtGrid', 'gridService'];

    function gridDirective($rootScope, $document, grid, gridService) {
        return {
            restrict: "EA",
            template: "Test",
            controller : "dtGridCtrl",
            link: function (scope, element, attrs, ctrl) {
                var instance = grid.init();
                instance.load();
            }
        }
    }
})();