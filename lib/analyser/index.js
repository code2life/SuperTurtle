var cron = require('../../common/cron').cronProcess;
var async = require('async');
var filter = require('./filter').filter;

var analyzer = function(config,callback){
    var _self = this;
    if(config.mode && (!config.executorSeries || !config.meta)){
        console.log('wrong analyzer config, plz check your settings.');
        return;
    }
    this.meta = config.meta;
    this.parallelMode =  config.mode;
    this.executorSeries = config.executorSeries;

    this.scope = config.scope;

    /* inject Mongo API */
    this.mongo = config.mongo;

    /* regular data operations, synchronize not required */
    config.cron.worker = config.method;
    config.cron.scope = config.scope;
    config.cron.scope.EasyMongo = config.mongo;
    new cron(config.cron);  //default auto start cron
    callback && callback(_self);
    console.log("Analyzer : analyzer initialized.");
    return this;
};

analyzer.prototype.analyseProcess = function(data) {
    var _self = this;
    if(this.parallelMode) {
        // exe one by one, hand over data to different executors
        console.log("Analyzer : analyzer progress start [MongoDB Mode]");
        console.log("Analyzer : execute on collection [" + this.meta + "]");

        _self.mongo.find('meta',{collectionName : _self.meta}, function(err,docs){
            if(err || docs.length == 0) console.log("Analyzer : DB error in persistence progress.");
            else {
                data.forEach(function(segment){
                    var persistence = {};
                    var temp = segment;
                    _self.executorSeries.forEach(function(mapper){
                        // must be synchronized method.
                        if(!!mapper.filter){
                            var afterFilter = filter(segment,mapper.filter);
                            persistence[mapper.col] = mapper.method.call(_self.scope,temp,afterFilter);
                        } else {
                            persistence[mapper.col] = mapper.method.call(_self.scope,temp);
                        }
                    });
                    docs[0].columns.forEach(function(e){
                        if(!persistence[e.columnName]) {
                            if(e.defaultValue) {
                                if(e.defaultValue.code) {
                                    persistence[e.columnName] = Date.now();
                                } else {
                                    persistence[e.columnName] = e.defaultValue;
                                }
                            } else {
                                persistence[e.columnName] = null;
                            }
                        }
                    });

                    // save data
                    _self.mongo.add(_self.meta,persistence,function(err){
                        if(err) {
                            console.log("Analyzer : DB error in persistence progress.");
                            console.log(err);
                        } else {
                            console.log("Analyzer : Data segment saved.");
                        }
                    });
                });
            }
        });
    } else {
        console.log("Analyzer : analyzer progress start [Console Mode]");
        console.log(data);
        console.log("Analyzer : DATA output finished.");
    }
};

exports.Analyzer = analyzer;